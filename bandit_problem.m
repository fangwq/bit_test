clear;
clc;
close all;
n = 10;
arms =  rand(1,10);
iter = 500;
eps = 0.1;
figure;
for i = 1:iter,
    if rand() > eps,
        choice = bestArm(av);
        thisAV = [choice, reward(arms[choice])]
        av = [av, thisAV];
    else
        choice = find(arms == np.random.choice(arms))
        thisAV = np.array([[choice, reward(arms[choice])]])
        av = [av, thisAV];
    end
    percCorrect = 100*(length(av[np.where(av[:,0] == np.argmax(arms))])/length(av));
    runningMean = mean(av);
    plot(i, runningMean, 'o');
    hold on;
end

function result = bestArm(a)
    bestArm = 0;
    bestMean = 0
    for u = a,
        avg = mean(a(a(:,0) == u[0]));
        if bestMean < avg,
            bestMean = avg
            bestArm = u(0);
        end
    end
end